%global dconf_version 0.26.1
%global glib2_version 2.55.1
%global gtk3_version 3.22.27

Name:           dconf-editor
Version:        3.38.3
Release:        1
Summary:        Graphical editor for dconf

License:        GPLv3
URL:            https://wiki.gnome.org/Projects/dconf
Source0:        https://download.gnome.org/sources/dconf-editor/3.38/dconf-editor-%{version}.tar.xz

BuildRequires:  /usr/bin/appstream-util desktop-file-utils gettext meson vala
BuildRequires:  pkgconfig(dconf) >= %{dconf_version} pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gmodule-2.0) pkgconfig(gtk+-3.0) >= %{gtk3_version} pkgconfig(libxml-2.0)

Requires:       dconf >= %{dconf_version} glib2 >= %{glib2_version} gtk3 >= %{gtk3_version}
Conflicts:      dconf <= 0.23.1

%description
This package provides a graphical tool for editing the dconf database.

%package_help

%prep
%autosetup -p1

%build
%meson
%meson_build

%install
%meson_install

%find_lang dconf-editor

%check
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/ca.desrt.dconf-editor.appdata.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/ca.desrt.dconf-editor.desktop

%files -f dconf-editor.lang
%license COPYING
%{_bindir}/dconf-editor
%{_datadir}/applications/ca.desrt.dconf-editor.desktop
%{_datadir}/bash-completion/
%{_datadir}/dbus-1/services/ca.desrt.dconf-editor.service
%{_datadir}/glib-2.0/schemas/ca.desrt.dconf-editor.gschema.xml
%{_datadir}/icons/hicolor/*/apps/ca.desrt.dconf-editor.png
%{_datadir}/icons/hicolor/scalable/apps/ca.desrt.dconf-editor-symbolic.svg
%{_datadir}/metainfo/ca.desrt.dconf-editor.appdata.xml
%{_datadir}/icons/hicolor/scalable/actions/ca.desrt.dconf-editor.small-rows-symbolic.svg
%{_datadir}/icons/hicolor/scalable/actions/ca.desrt.dconf-editor.big-rows-symbolic.svg

%files help
%{_mandir}/man1/dconf-editor.1*

%changelog
* Wed Jun 30 2021 caodongxia <caodongxia@huawei.com> - 3.38.3-1
- Package upgrade to 3.38.3

* Tue Jun 30 2020 lizhenhua <lizhenhua21@huawei.com> - 3.36.2-1
- Package upgrade to 3.36.2

* Sat Jun 20 2020 wangchong <wangchong56@huawei.com> - 3.34.4-1
- Package upgrade

* Tue Nov 19 2019 mengxian <mengxian@huawei.com> - 3.30.2-2
- Package init
